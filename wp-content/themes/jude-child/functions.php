<?php
/**
 * Child-Theme functions and definitions
 */

function jude_child_scripts() {
    wp_enqueue_style( 'jude-parent-style', get_template_directory_uri(). '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'jude_child_scripts' );

?>
