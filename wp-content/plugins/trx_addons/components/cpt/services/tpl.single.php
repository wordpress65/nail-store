<?php
/**
 * The template to display the service's single page
 *
 * @package WordPress
 * @subpackage ThemeREX Addons
 * @since v1.4
 */

get_header();

while ( have_posts() ) { the_post();
	?>
	<article id="post-<?php the_ID(); ?>" <?php post_class( 'services_single itemscope' ); trx_addons_seo_snippets('', 'Article'); ?>>
		<?php

		// Post content
		?>
		<section class="services_page_content entry-content"<?php trx_addons_seo_snippets('articleBody'); ?>>
		<?php
			the_content( );
		?>
		<div>
		<p class="sc_align_center"><span class="loud-text">Contactez-nous<br>
				<strong>Tél.&nbsp;<a href="tel:0636838762">06 36 83 87 62</a></strong></span></p>
		</div>

		<div class="sc_item_button sc_button_wrap sc_align_center">
			<a href="https://annabellenailart.net/prendre-rendez-vous/" id="sc_button_242540539" class="sc_button color_style_dark sc_button_default sc_button_size_normal sc_button_icon_left sc_button_hover_slide_top sc_button_hover_style_dark">
				<span class="sc_button_text">
					<span class="sc_button_title">Prendre rendez-vous</span>
				</span><!-- /.sc_button_text -->
			</a><!-- /.sc_button -->
			<a href="mailto:pdieuanh89@gmail.com" class="sc_button color_style_dark sc_button_default sc_button_size_normal sc_button_icon_left sc_button_hover_slide_top sc_button_hover_style_dark">
				<span class="sc_button_text">
					<span class="sc_button_title">Envoyer un message</span>
				</span><!-- /.sc_button_text -->
			</a><!-- /.sc_button -->
		</div>
		</section><!-- .entry-content -->
		<?php

		// Link to the product
		if (!empty($meta['product']) && (int) $meta['product'] > 0) {
			?><div class="services_page_buttons">
				<a href="<?php echo esc_url(get_permalink($meta['product'])); ?>" class="sc_button theme_button"><?php esc_html_e('Order now', 'trx_addons'); ?></a>
			</div><?php
		}

		do_action('trx_addons_action_after_article', 'services.single');

	?></article><?php

	// Related items (select dishes with same category)
	$taxonomies = array();
	$terms = get_the_terms(get_the_ID(), TRX_ADDONS_CPT_SERVICES_TAXONOMY);
	if ( !empty( $terms ) ) {
		$taxonomies[TRX_ADDONS_CPT_SERVICES_TAXONOMY] = array();
		foreach( $terms as $term )
			$taxonomies[TRX_ADDONS_CPT_SERVICES_TAXONOMY][] = $term->term_id;
	}

	$trx_addons_related_style   = explode('_', trx_addons_get_option('services_style'));
	$trx_addons_related_type    = $trx_addons_related_style[0];
	$trx_addons_related_columns = empty($trx_addons_related_style[1]) ? 1 : max(1, $trx_addons_related_style[1]);
	
	trx_addons_get_template_part('templates/tpl.posts-related.php',
										'trx_addons_args_related',
										apply_filters('trx_addons_filter_args_related', array(
															'class' => 'services_page_related sc_services sc_services_'.esc_attr($trx_addons_related_type),
															'posts_per_page' => $trx_addons_related_columns,
															'columns' => $trx_addons_related_columns,
															'template' => TRX_ADDONS_PLUGIN_CPT . 'services/tpl.'.trim($trx_addons_related_type).'-item.php',
															'template_args_name' => 'trx_addons_args_sc_services',
															'post_type' => TRX_ADDONS_CPT_SERVICES_PT,
															'taxonomies' => $taxonomies
															)
													)
									);

	// If comments are open or we have at least one comment, load up the comment template.
	if ( comments_open() || get_comments_number() ) {
		comments_template();
	}
}

get_footer();
?>